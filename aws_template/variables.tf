variable "customer_full_name" {}
variable "customer_short_name" {}
variable "tier" {}
variable "cidr_block" {}
variable "region"     {}

# S3
variable "s3_base_bucket_name" {}

# EC2
variable "instance_size" {
  type = map
}
variable "amis" {
  type = map
}
variable "alb_account_id" {}
variable "edge_target_group_name" {}

# Cloudfront
variable "cloudfront_price_class" {}

# Subnets
variable "subnet_private_a" {}
variable "subnet_private_b" {}
variable "subnet_public_a" {}
variable "subnet_public_b" {}

# Autoscaling
variable "asg_size" {
  type = map
}
variable "devops_admin" {}
variable "devops_admin_public_ip" {}
variable "devops_admin_pubkey" {}
variable "aws_profile" {}
# RDS
variable "db_engine_version" {}
variable "db_identifier" {}
variable "db_name" {}
variable "db_password" {}
variable "db_parameter_group" {}
variable "db_allocated_storage" {}
