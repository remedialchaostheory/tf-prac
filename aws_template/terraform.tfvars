#this section should have been changed in the template personalization sed section.
customer_full_name  = "ORGANIZATION NAME"
customer_short_name = "mytestorg"
devops_admin = "usopp"
devops_admin_pubkey = "ssh-rsa DONOTUSEaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06DONTUSETHIS nobody@nowhere"
#change this to the public IP of your devops admins
devops_admin_public_ip = "204.102.74.42/32"
aws_profile = "joe-ninja-ops"
tier = "stage"
cidr_block          = "10.11.0.0/16"
#change this to the desired region for you infrastructure
region              = "us-west-1"
s3_base_bucket_name      = "mytestorg-assets"
cloudfront_price_class = "PriceClass_100"
subnet_private_a = "10.11.1.0/24"
subnet_private_b = "10.11.2.0/24"
subnet_public_a = "10.11.3.0/24"
subnet_public_b = "10.11.4.0/24"

#reate hardened AMIs and set the id's here
amis = {
  "bastion" = "ami-079d4df4be8637b79"
  "asg" = "ami-075a325c3f77376e4"
}

instance_size = {
  "bastion" = "t2.micro"
  "asg" = "t2.micro"
  "db" = "db.t2.micro"
}

asg_size = {
  "max" = "2"
  "min" = "2"
  "desired" = "2"
}
#set this too
alb_account_id = "985666609251"
edge_target_group_name = "Edge-Frontends"

db_engine_version = "5.6.41"
db_identifier = "mytestorg"
db_name = "mytestorg"
#Set this to something, review with a security pass to put this in an encrypted parameter store variable
db_password = "xxxxxxxx"
db_parameter_group = "default.mysql5.6"
db_allocated_storage = "10"
