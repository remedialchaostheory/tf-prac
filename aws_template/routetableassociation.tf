resource "aws_route_table_association" "mytestorg-public-1a-rta" {
    route_table_id = aws_route_table.mytestorg-public-1a.id
    subnet_id = aws_subnet.mytestorg-public-1a.id
}

resource "aws_route_table_association" "mytestorg-public-1b-rta" {
    route_table_id = aws_route_table.mytestorg-public-1b.id
    subnet_id = aws_subnet.mytestorg-public-1b.id
}

resource "aws_route_table_association" "mytestorg-private-1a-rta" {
    route_table_id = aws_route_table.mytestorg-private-1a.id
    subnet_id = aws_subnet.mytestorg-private-1a.id
}

resource "aws_route_table_association" "mytestorg-private-1b-rta" {
    route_table_id = aws_route_table.mytestorg-private-1b.id
    subnet_id = aws_subnet.mytestorg-private-1b.id
}
