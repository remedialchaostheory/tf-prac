# Bastion
resource "aws_instance" "bastion01" {
    ami                         = var.amis["bastion"]
    availability_zone           = "${var.region}a"
    instance_type               = var.instance_size["bastion"]
    monitoring                  = false
    subnet_id                   = aws_subnet.mytestorg-public-1a.id
    vpc_security_group_ids      = [aws_security_group.usopp.id]
    associate_public_ip_address = true
    source_dest_check           = true
    key_name                    = "usopp_key"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 16
        delete_on_termination = true
    }
    tags = {
        Client = "mytestorg"
        Name = "usopp bastion01"
    }
}

resource "aws_eip" "bastion01" {
    vpc   = true
    tags = {
        Name = "bastion01"
    }
}

resource "aws_eip_association" "bastion01_eip_assoc" {
    instance_id = aws_instance.bastion01.id
    allocation_id = aws_eip.bastion01.id
}

output "bastion01_ip_addr" {
    value = aws_eip.bastion01.public_ip
}
