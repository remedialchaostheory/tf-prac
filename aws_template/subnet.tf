resource "aws_subnet" "mytestorg-public-1a" {
    vpc_id                  = aws_vpc.mytestorg.id
    cidr_block              = var.subnet_public_a
    availability_zone       = "${var.region}a"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.customer_full_name} Public 1a"
    }
}
resource "aws_subnet" "mytestorg-public-1b" {
    vpc_id                  = aws_vpc.mytestorg.id
    cidr_block              = var.subnet_public_b
    availability_zone       = "${var.region}b"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.customer_full_name} Public 1b"
    }
}

resource "aws_subnet" "mytestorg-private-1a" {
    vpc_id                  = aws_vpc.mytestorg.id
    cidr_block              = var.subnet_private_a
    availability_zone       = "${var.region}a"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.customer_full_name} Private 1a"
    }
}
resource "aws_subnet" "mytestorg-private-1b" {
    vpc_id                  = aws_vpc.mytestorg.id
    cidr_block              = var.subnet_private_b
    availability_zone       = "${var.region}b"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.customer_full_name} Private 1b"
    }
}
