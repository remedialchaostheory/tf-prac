resource "aws_route_table" "mytestorg-public-1a" {
    vpc_id     = aws_vpc.mytestorg.id

    route {
        cidr_block = "0.0.0.0/0"
        #TODO create igw in terraform and bind to route
        gateway_id = aws_internet_gateway.mytestorg.id
    }

    tags = {
        Name = "mytestorg public 1a"
    }
}
resource "aws_route_table" "mytestorg-public-1b" {
    vpc_id     = aws_vpc.mytestorg.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.mytestorg.id
    }

    tags = {
        Name = "mytestorg public 1b"
    }
}

resource "aws_route_table" "mytestorg-private-1a" {
    vpc_id     = aws_vpc.mytestorg.id

    tags = {
        Name = "mytestorg private 1a"
    }
}

resource "aws_route_table" "mytestorg-private-1b" {
    vpc_id     = aws_vpc.mytestorg.id

    tags = {
        Name = "mytestorg private 1b"
    }
}
