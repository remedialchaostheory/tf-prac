resource "aws_security_group" "usopp" {
    name        = "usopp"
    description = "Security group for usopp access"
    vpc_id      = aws_vpc.mytestorg.id

#inbound admin whitelist
    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        #inbound ssh from: devops admins
        cidr_blocks     = [var.devops_admin_public_ip]
        self            = true
    }
#icmp from the office and vpn for ping
    ingress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = [var.devops_admin_public_ip]
        self            = true
    }
#wide open egress
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags = {
        Name = "usopp access"
    }
}
resource "aws_security_group" "edge-frontends-instances" {
    name        = "edge-frontends-instances"
    description = "Security group for edge-frontends instances"
    vpc_id      = aws_vpc.mytestorg.id

#inbound 80 from the alb and usopp
    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        security_groups = [aws_security_group.edge-frontends-alb.id,aws_security_group.usopp.id]
    }
#inbound 443 from the alb and usopp
    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        security_groups = [aws_security_group.edge-frontends-alb.id,aws_security_group.usopp.id]
    }
#inbound 22 from usopp
    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        security_groups = [aws_security_group.usopp.id]
        self            = true
    }
#wide open egress
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
    tags = {
        Name = "Edge Frontends instances"
    }
}

resource "aws_security_group" "edge-frontends-alb" {
    name        = "edge-frontends-alb"
    description = "Security group for edge-frontends ALB"
    vpc_id      = aws_vpc.mytestorg.id

#inbound 80 from the alb
    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        cidr_blocks     = ["67.164.84.151/32"]
    }
#inbound 443 from the alb
    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["67.164.84.151/32"]
        security_groups = [aws_security_group.usopp.id]
    }
#wide open egress
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }
#inbound 80 from the usopp
    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        security_groups = [aws_security_group.usopp.id]
    }

    tags = {
        Name = "${var.customer_full_name} ALB"
    }
}
resource "aws_security_group" "mytestorg-rds" {
    name        = "mytestorg-RDS"
    description = "Security group for ${var.customer_full_name} RDS"
    vpc_id      = aws_vpc.mytestorg.id

    ingress {
        from_port       = 3306
        to_port         = 3306
        protocol        = "tcp"
        security_groups = [aws_security_group.usopp.id,aws_security_group.edge-frontends-instances.id]
        self            = false
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags = {
        Name = "${var.customer_full_name} RDS"
    }
}
