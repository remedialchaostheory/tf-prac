resource "aws_internet_gateway" "mytestorg" {
  vpc_id = aws_vpc.mytestorg.id

  tags = {
    Name = var.customer_full_name
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_eip" "nat" {
  vpc      = true
  tags = {
    Name = var.customer_full_name
  }
}

resource "aws_nat_gateway" "mytestorg" {
  #this is the elastic ip association
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.mytestorg-public-1a.id
  depends_on    = [aws_internet_gateway.mytestorg]
  tags = {
    Name = var.customer_full_name
  }
  lifecycle {
    create_before_destroy = true
  }
}
