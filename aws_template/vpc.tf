resource "aws_vpc" "mytestorg" {
    cidr_block           = var.cidr_block
    enable_dns_hostnames = true
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags = {
        Client = var.customer_full_name
        Name = var.customer_full_name
    }
}
