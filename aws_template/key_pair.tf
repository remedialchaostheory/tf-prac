resource "aws_key_pair" "usopp_key" {
  key_name   = "usopp_key"
  public_key = var.devops_admin_pubkey
}
