resource "aws_db_instance" "mytestorg" {
    identifier                = var.db_identifier
    allocated_storage         = var.db_allocated_storage
    storage_type              = "gp2"
    engine                    = "mysql"
    engine_version            = var.db_engine_version
    instance_class            = var.instance_size["db"]
    name                      = var.db_name
    username                  = var.db_name
    password                  = var.db_password
    port                      = 3306
    publicly_accessible       = false
    security_group_names      = []
    vpc_security_group_ids    = [aws_security_group.mytestorg-rds.id]
    db_subnet_group_name      = aws_db_subnet_group.mytestorg.name
    parameter_group_name      = var.db_parameter_group
    multi_az                  = true
    backup_retention_period   = 7
    backup_window             = "04:08-04:38"
    maintenance_window        = "sat:07:39-sat:08:09"
    final_snapshot_identifier = "${var.db_identifier}-final"
}

output "rds_db_endpoint" {
    value = aws_db_instance.mytestorg.endpoint
}
