resource "aws_db_subnet_group" "mytestorg" {
    name        = "mytestorg"
    description = "mytestorg subnet group"
    subnet_ids  = [aws_subnet.mytestorg-private-1a.id,aws_subnet.orgname-private-1b.id]
}
