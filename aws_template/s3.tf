data "aws_elb_service_account" "main" {}

resource "aws_s3_bucket" "mytestorg" {
  bucket = "${var.s3_base_bucket_name}-${var.tier}"
  acl = "private"
  force_destroy = true

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Name = "${var.customer_full_name} Assets"
  }

  policy = <<EOF
{
  "Version" : "2012-10-17",
  "Statement" : [
    {
      "Sid" : "AddPerm",
      "Effect" : "Allow",
      "Principal" : "*",
      "Action" : ["s3:GetObject"],
      "Resource" : ["arn:aws:s3:::${var.s3_base_bucket_name}-${var.tier}/*"]
    }
  ]
}
  EOF
}

locals {
  s3_origin_id = "myS3Origin"
}

resource "aws_s3_bucket" "mytestorg_elb_logs" {
  bucket = "mytestorg-logs-${var.tier}"
  acl = "private"
  force_destroy = true
  tags = {
    Name = "LB Logs"
  }

#   policy = <<EOF
# {
#   "Version" : "2012-10-17",
#   "Statement" : [
#     {
#       "Effect" : "Allow",
#       "Principal" : {
#         "AWS" : [data.aws_elb_service_account.main.arn]
#       },
#       "Action" : ["s3:PutObject"],
#       "Resource" : ["arn:aws:s3:::mytestorg-logs-${var.tier}/*"]
#     }
#    ] 
# }
#   EOF
}
