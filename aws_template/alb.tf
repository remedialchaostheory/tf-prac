resource "aws_lb" "frontends" {
    name                        = "Edge-frontends"
    load_balancer_type          = "application"
    subnets                     = [aws_subnet.mytestorg-public-1a.id,aws_subnet.orgname-public-1b.id]
    security_groups             = [aws_security_group.edge-frontends-alb.id]
    enable_deletion_protection  = false
    enable_cross_zone_load_balancing = true
    idle_timeout                = 60
    internal                    = false
    depends_on = [aws_s3_bucket.mytestorg_elb_logs, aws_internet_gateway.orgname]

    access_logs {
        bucket        = "mytestorg-logs-${var.tier}"
        prefix = "frontends"
        enabled       = true
    }

    tags = {
        Client = "mytestorg"
    }
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.frontends.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.alb_target.arn
    type = "forward"
  }
}
resource "aws_lb_listener" "https_listener" {
  load_balancer_arn = aws_lb.frontends.arn
  port              = 443
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.alb_target.arn
    type = "forward"
  }
}

resource "aws_alb_target_group" "alb_target" {
  name = var.edge_target_group_name
  port = "80"
  protocol = "HTTP"
  vpc_id = aws_vpc.mytestorg.id
  tags = {
    name = var.edge_target_group_name
  }
  stickiness {
    type  = "lb_cookie"
    cookie_duration = 1800
    enabled = true
  }
}
