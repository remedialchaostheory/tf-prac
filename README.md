# aws_terraform_template

## Template for base AWS infrastructure

This template would allow you to create a base AWS deployment that is:

* secure
* scalable
* reliable
* cost-effective

## Step 0 -> harden your AWS root account

<https://docs.aws.amazon.com/IAM/latest/UserGuide/id_root-user.html>
Proceed with an Admin IAM user NOT the root credentials
<https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html>

## Step 1

Install terraform
<https://www.terraform.io/intro/getting-started/install.html>

> Note: when it comes time to apply your infrastructure, Terraform will look at everything in the current working directory that ends with the .tf extension
> You could set components aside for later utilization (such as S3 or RDS) by simply changing the extension.  

## Step 2

> Change the content in your `variables.tf` file:

1. Set your profile
2. Set your desired region (us-east-1 is reston va, the most common aws location)
<https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions>
3. Set your pubkey (for ssh access)
4. Put your (NON ROOT!!!) credentials in ~/.aws/config
5. Fix the path in your provider.tf file to reference the ~/.aws/config file
6. Set your base AMI (machine images)

> Note: you can use default images of your choosen platform or you can spin up the default image, harden it and give it a good base config and then snapshot the image.

## Step 3 -> use `sed` to replace template placeholders with company names  

### Linux users

> Change out the org name with your org or service and the company long name  

```bash
sed -i 's:orgname:yourorg:' *
sed -i 's:Organization Longname:Your Orgname:' *
sed -i 's:DEVOPS_ADMIN:your_devops_admins:' *
```

### Mac Users

> NOTE: `sed` behaves differently on `mac`. backup file extension is required and delimeter is `/` instead of `:`.  

```bash
sed -i '.bak' 's/orgname/yourorg/' *
sed -i '.bak' 's/Organization Longname/Your Company Name/' *
sed -i '.bak' 's/DEVOPS_ADMIN/your_devops_admins/' *
```
